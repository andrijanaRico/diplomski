

// SelectText
function SelectText(element) {
  var doc = document,
      text = element,
      range, selection;

  if (doc.body.createTextRange) {
      range = document.body.createTextRange();
      range.moveToElementText(text);
      range.select();
      /*
      if(range.toString().length === 0){
        range.moveToElementText(text);
        range.select();
      }
      */
  } else if (window.getSelection) {
      selection = window.getSelection();
      if(selection.toString().length === 0){
        range = document.createRange();
        range.selectNodeContents(text);
        selection.removeAllRanges();
        selection.addRange(range);
      }
  }
}

function selectImage(event) {
    var img = $(event.target).attr('src');
    $('.replace-image img').css('display', 'none').attr('src', img).fadeIn('slow');
    //alert(img);
}


var siteUrl = 'https://aleksandravasic.000webhostapp.com/';
//var siteUrl = 'http://localhost:63342/HTML/';

// SelectText
function SelectText(element) {
    var doc = document,
        text = element,
        range, selection;

    if (doc.body.createTextRange) {
        range = document.body.createTextRange();
        range.moveToElementText(text);
        range.select();
        /*
        if(range.toString().length === 0){
          range.moveToElementText(text);
          range.select();
        }
        */
    } else if (window.getSelection) {
        selection = window.getSelection();
        if(selection.toString().length === 0){
            range = document.createRange();
            range.selectNodeContents(text);
            selection.removeAllRanges();
            selection.addRange(range);
        }
    }
}

function selectImage(event) {
    var img = $(event.target).attr('src');
    $('.replace-image img').css('display', 'none').attr('src', img).fadeIn('slow');
}

function sendMail() {
    console.log('send mail');
    var name = $('#name').val(),
        email = $('#email').val(),
        message = $('#message').val();
    console.log(name);
    console.log(email);
    console.log(message);
    if (name.length && email.length && message.length) {
        $.ajax({
            url: siteUrl + 'includes/sendMail.php',
            type: 'post',
            data: {name: name, email: email, message: message},
            success: function (data) {
                console.log(data);
                $('#mail-response').html(data).css('color', 'green');
                $('#name, #email, #message').val('');
            },
            error: function (error) {
                $('#mail-response').html(error).css('color', 'red');
            }
        });
    } else {
        $('#mail-response').html('You must fill in all the fields').css('color', 'red');
    }
}